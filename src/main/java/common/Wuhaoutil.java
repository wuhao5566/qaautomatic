package common;

import java.io.IOException;
import java.util.Properties;

public class Wuhaoutil {
    public  Properties pro() throws IOException {
        Properties info=new Properties();
        info.load(this.getClass().getClassLoader().getResourceAsStream("testdata.properties"));
        return info;
    }
}
