package tests.Curriculum;

import common.Deiver;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.*;

import java.util.Set;

public class CouresCreate2 {
    public static WebDriver driver;
    public static Loginpage loginpage;
    private Homepage homepage;
    private Trainpage trainpage;
    private TrainTruepage trainTruepage;
    private Createcurriculumpage createcurriculumpage;
    private Courpage courpage;
    /*
     * 验证账号是否有讲师权限权限
     *
     * 登录的为教师用户
     *
     * 验证教师用户创建课程
     * */
    @Test
    public void Curriculum_creation() throws InterruptedException {
        loginpage.Login("wuhao","123456");

        //获得当前窗口句柄
        String search_handle = driver.getWindowHandle();
        //点击创建课程
        trainTruepage.click_Create_Curriculum();
        Thread.sleep(2000);
        //获得所有窗口句柄
        Set<String> handles = driver.getWindowHandles();
        //判断是否为注册窗口， 并操作注册窗口上的元素
        for(String handle : handles) {
            if (handle.equals(search_handle) == false) {
                //切换到注册页面
                driver.switchTo().window(handle);
                System.out.println("now register window!");
                Thread.sleep(2000);
                String Course_title = "吴昊的自动化";
                //输入课程标题
                createcurriculumpage.imput_Course_title(Course_title);
                Thread.sleep(2000);
            }
            //测试点
        }
        //点击创建
        createcurriculumpage.click_create();
        Thread.sleep(5000);
        //发布课程
        courpage.click_Release_course();
        Thread.sleep(3000);
        //浏览器操作alter弹出框
        //返回一个Alert（弹窗）对象。
        Alert alert=driver.switchTo().alert();
        alert.accept();
        System.out.println("点击确认按钮完成");
        Thread.sleep(3000);
        //关闭浏览器
        driver.close();
    }
    /*
     *   1.退出登录------home选项后去首页退出登录
     * 2.学员登录，查看此课程，加入学习
     *   测试的自动化，一个用例，对应一个test不会一窗到底，结束后就关闭
     *
     * */
    /*
     *在套件中的所有测试运行之前运行
     * */
    @BeforeSuite()
    public void beforeSuite() throws InterruptedException {
        /*
         * 这一块可以封装成一个类,以封装
         * */
        driver=new Deiver().Load_driver("webdriver.chrome.driver","C:/driver/chromedriver.exe","http://qa2.edusoho.cn/");
        ///  一次当前页面元素是否存在，如果超过设置时间检测不到则抛出异常
        WebDriverWait wait=new WebDriverWait(driver,10,1);
        /////创建鼠标属性方法,--------------然后获取id为kw的元素并点击:action.moveToElement(driver.findElement(By.id("kw"))).perform();。
        Actions actions=new Actions(driver);
        //等待
        Thread.sleep(2000);
        //初始化page页面(注意：要放在打开浏览器之后)
        loginpage= PageFactory.initElements(driver, Loginpage.class);
        homepage=PageFactory.initElements(driver, Homepage.class);
        trainpage=PageFactory.initElements(driver, Trainpage.class);
        trainTruepage=PageFactory.initElements(driver, TrainTruepage.class);
        createcurriculumpage=PageFactory.initElements(driver, Createcurriculumpage.class);
        courpage=PageFactory.initElements(driver,Courpage.class);
    }

}
