package tests.LoginUser;

import common.Deiver;
import common.Wuhaoutil;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.Loginpage;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class Logintest {
    public static WebDriver driver;
    public static Loginpage loginpage;

    /*
     *在套件中的所有测试运行之前运行
     * */
    @BeforeSuite()
    public void beforeSuite() throws InterruptedException {
        /*
         * 这一块可以封装成一个类,以封装
        * */
        driver=new Deiver().Load_driver("webdriver.chrome.driver","C:/driver/chromedriver.exe","http://qa2.edusoho.cn/");
        ///  一次当前页面元素是否存在，如果超过设置时间检测不到则抛出异常
        WebDriverWait wait=new WebDriverWait(driver,10,1);
        /////创建鼠标属性方法,--------------然后获取id为kw的元素并点击:action.moveToElement(driver.findElement(By.id("kw"))).perform();。
        Actions actions=new Actions(driver);
        //等待
        Thread.sleep(2000);
        //初始化page页面(注意：要放在打开浏览器之后)
       loginpage=PageFactory.initElements(driver,Loginpage.class);
    }

    /*
     *在套件中的所有测试运行之后运行
     * */
    @AfterSuite()
    public void AfterSuite() throws InterruptedException {
        driver.close();
    }
    @Test
    public void loginuser() throws InterruptedException, IOException {
        //登录
        Properties pro=new Wuhaoutil().pro();
        loginpage.Login(pro.getProperty("student.Name"),pro.getProperty("student.PWD"));
        //获取cookie
        Set<Cookie> data=driver.manage().getCookies();

        System.out.print(data);
        System.out.print("cookie个数"+driver.manage()
        .getCookies().size());

    }



}
