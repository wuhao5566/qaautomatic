package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
* 课程任务列表页面
* */
public class Taskspage extends Courpage{
    /*
    *添加任务
    * */
    @FindBy(id = "step-3")
    public WebElement Add_task;
    /*
     *添加图文任务
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[1]/a")
    public WebElement Image_Text;
    /*
     *视频
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[2]/a")
    public WebElement video;
    /*
     *音乐
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[3]/a")
    public WebElement audio_frequency;
    /*
     *直播
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[4]/a")
    public WebElement Live_roadcast;
    /*
     *讨论
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[5]/a")
    public WebElement discuss;
    /*
     *Flash
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[6]/a")
    public WebElement Flash;
    /*
     *文档
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[7]/a")
    public WebElement File;
    /*
     *PPt
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[8]/a")
    public WebElement PPT;
    /*
     *考试
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[9]/a")
    public WebElement Examination;
    /*
     *作业
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[10]/a")
    public WebElement task;
    /*
     *练习
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[11]/a")
    public WebElement Practice;
    /*
     *下载资料
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[12]/a")
    public WebElement Downloading_date;
    /*
     *教评文卷
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[13]/a")
    public WebElement Teacher_evaluation;
    /*
     *share,添加嵌入代码
     * */
    @FindBy(xpath = "//*[@id=\"step1-form\"]/ul/li[14]/a")
    public WebElement share;

    /*
     *下一步
     * */
    @FindBy(id = "course-tasks-next")
    public WebElement next_step;
    /*
     *上一步
     * */
    @FindBy(id = "course-tasks-prev")
    public WebElement Previous_step;
    /*
     *保存
     * */
    @FindBy(id = "course-tasks-submit")
    public WebElement Preservation;
    ///-----------------------------图文---------------------------
    /*
     *标题名称
     * */
    @FindBy(id = "title")
    public WebElement title;
    /*
     *iframe内容
     * */
    @FindBy(id = "cke_text-content-field")
    public WebElement iframe;
    /*
     *观看时长
     * */
    @FindBy(id = "finishDetail")
    public WebElement finishDetail;
    /*
     *设置选修
     * */
    @FindBy(id = "//*[@id=\"step3-form\"]/div[3]/div/label/input")
    public WebElement setting_Elective;
    //--------------------------------视频
    /*
     *添加本地视频路径
     * */
    @FindBy(xpath = "//*[@id=\"rt_rt_1dm0hsark1l8rjdo1bkq1gnuq4d1\"]/input")
    public WebElement filevideo;
    //---------------------------------讨论
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;
//    /*
//     *添加任务
//     * */
//    @FindBy(id = "step-3")
//    public WebElement Add_task;

}
