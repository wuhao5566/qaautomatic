package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
* 登录页
* */
public class Loginpage {
    /*
    * 用户名
    * */
    @FindBy(id = "login_username")
    public WebElement username;
    /*
     * 用户密码
     * */
    @FindBy(id = "login_password")
    public WebElement userpassword;
    /*
     * 登录
     * xpath://*[@id="login-form"]/div[3]/button
     * */
    @FindBy(xpath = "//*[@id=\"login-form\"]/div[3]/button")
    public WebElement loginsubmit;
    /*
     * 5天内自动登录
     * xpath://*[@id="login-form"]/div[4]/div/input
     * */
    @FindBy(xpath = "//*[@id=\"login-form\"]/div[4]/div/input")
    public WebElement automatic_logon;
    /*
     * 找回密码
     * xpath://*[@id="login-form"]/div[4]/div/a
     * */
    @FindBy(xpath = "//*[@id=\"login-form\"]/div[4]/div/a")
    public WebElement Retrieve_password;
    /*
    * 输入用户名
    * */
    public void inputname(String usernames){
        System.out.print(usernames);
        username.clear();
        username.sendKeys(usernames);
    }
    /*
     * 输入用户名
     * */
    public void inputpassword(String passwords){
        System.out.print(passwords);
        userpassword.clear();
        userpassword.sendKeys(passwords);
    }
    /*
     * 点击登录
     * */
    public void clicklogin(){
        System.out.print("登录了");
        loginsubmit.click();
    }
    /*
     * 取消或者勾选5天自动登录登录
     * */
    public void automatic_login(){
        System.out.print("取消或者勾选5天自动登录登录");
        automatic_logon.click();
    }
    /*
     *点击忘记密码
     * */
    public void Retrieve_pass(){
        System.out.print("密码忘了");
        Retrieve_password.click();
    }
    //登录方法（直接调用）
    public void Login(String uesrname,String password){
        inputname(uesrname);
        inputpassword(password);
        clicklogin();
    }
}
