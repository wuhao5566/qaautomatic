package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
* 创建课程页
* */
public class Createcurriculumpage {
    /*
    *普通课程
    * */
    @FindBy(xpath = "//*[@id=\"courseset-create-form\"]/div[2]/div[2]/div")
    public WebElement General_course;
    /*
     *直播课程
     * */
    @FindBy(xpath = "//*[@id=\"courseset-create-form\"]/div[2]/div[3]/div")
    public WebElement Live_broadcast_course;
    /*
     *所属部门
     * */
    @FindBy(xpath = "")
    public WebElement Subordinate_departments;
    /*
     *课程标题
     * */
    @FindBy(id = "course_title")
    public WebElement Course_title;
    /*
     *自由式学习
     * */
    @FindBy(xpath = "//*[@id=\"courseset-create-form\"]/div[5]/div[2]/label[1]/input")
    public WebElement Free_learning;
    /*
     *解锁式学习
     * */
    @FindBy(xpath = "//*[@id=\"courseset-create-form\"]/div[5]/div[2]/label[2]/input")
    public WebElement Unlocking_Learning;
    /*
     *创建
     * */
    @FindBy(id = "courseset-create-btn")
    public WebElement create;
    /*
     *取消
     * */
    @FindBy(xpath = "//*[@id=\"courseset-create-form\"]/div[7]/a")
    public WebElement cancel;
    //---------------------------------------方法--------------------------------
    /*
    *点击普通课程创建
    * */
    public void click_General_course(){
        General_course.click();
    }
    /*
     *点击直播课程创建
     * */
    public void click_Live_broadcast_course(){
        Live_broadcast_course.click();
    }
    /*
     *输入课程所属部门？？
     * */
    public void imput_Subordinate_departments(){

    }
    /*
     *输入课程标题
     * */
    public void imput_Course_title(String coursetitle){
        System.out.print(coursetitle);
       // Course_title.clear();
        Course_title.sendKeys(coursetitle);
    }
    /*
     *自由学习点击
     * */
    public void click_Free_learning(){
        Free_learning.click();
    }
    /*
     *解锁式学习
     * */
    public void click_Unlocking_Learning(){
        Unlocking_Learning.click();
    }
    /*
     *创建课程
     * */
    public void click_create(){
        create.click();
    }
    /*
     *取消创建课程
     * */
    public void click_cancel(){
        cancel.click();
    }

}
