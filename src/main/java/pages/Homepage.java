package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
* 前台头
* */
public class Homepage {
        /*
         *  我的学习
         * */
        @FindBy(xpath ="/html/body/div[1]/header/div/nav/div/ul/li[1]/a")
        public WebElement study;
        /*
         * 管理后台
         * */
        @FindBy(xpath = "/html/body/div[1]/header/div/nav/div/ul/li[2]/a")
        public WebElement Administration;
        /*
        *xpath:/html/body/div[1]/header/div/nav/div/ul/li[4]/ul/li[2]/a
        * 个人主页
        * */
        @FindBy(xpath = "/html/body/div[1]/header/div/nav/div/ul/li[4]/ul/li[2]/a")
        public WebElement Personal_homepage;
        /*
         *xpath:
         * 个人中心
         * */
        @FindBy(xpath = "/html/body/div[1]/header/div/nav/div/ul/li[4]/ul/li[3]/a")
        public WebElement Personal_core;
        /*
         *xpath:/html/body/div[1]/header/div/nav/div/ul/li[4]/ul/li[4]/a
         * 退出
         * */
        @FindBy(xpath = "/html/body/div[1]/header/div/nav/div/ul/li[4]/ul/li[4]/a")
        public WebElement exit;
         /*
        *鼠标悬浮操作，咋做
         * 搜索
         * */
         @FindBy(xpath = "/html/body/div[1]/header/div/nav/div/form/div/input")
         public WebElement search;
         //点击我的学习
         public void clik_study(){
             study.click();
         }
        //点击管理后台
        public void clik_Administration(){
            Administration.click();
        }
        //点击个人主页
        public void clik_homepage(){
            Personal_homepage.click();
        }
        //点击 个人中心
        public void clik_core(){
            Personal_core.click();
        }
        //点击退出
        public void clik_exit(){
            exit.click();
        }
        //输入搜索内容
        public void input_search(String content){
             System.out.print(content);
             search.clear();
             search.sendKeys(content);
            //search.click();
        }
}
