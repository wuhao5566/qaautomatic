package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
*后台头
*后台左侧栏
* */
public class Basepage {
        /*
        * 后台logo
        * */
        @FindBy(xpath ="/html/body/div[1]/div/div[1]/a")
        private WebElement homepage;
        /*
        * 首页
        * */
        @FindBy(xpath = "//*[@id=\"menu_admin_homepage\"]/a")
        private WebElement home_page;
        /*
         * 培训
         * */
        @FindBy(xpath = "//*[@id=\"menu_admin_train\"]/a")
        private WebElement Train;
        /*
         * 数据中心
         * */
        @FindBy(xpath = "//*[@id=\"menu_admin_data\"]/a")
        private WebElement Data_center;
        /*
         * 用户
         * */
        @FindBy(xpath = "//*[@id=\"menu_admin_user\"]/a")
        private WebElement Administration;
        /*
         * 运营
         * */
        @FindBy(xpath = "//*[@id=\"menu_admin_operation\"]/a")
        private WebElement user;
        /*
         * 教育云
         * */
        @FindBy(xpath = "//*[@id=\"menu_admin_app\"]/a")
        private WebElement Education_cloud;
        /*
         * 系统
         * */
        @FindBy(xpath = "//*[@id=\"menu_admin_system\"]/a")
        private WebElement Systems;
        /*
         * 退出
         * */
        @FindBy(xpath = "/html/body/div[1]/div/div[2]/ul[2]/li[1]/ul/li/a")
        private WebElement exit;
        /*
         * home图标
         * */
        @FindBy(xpath = "/html/body/div[1]/div/div[2]/ul[2]/li[3]/a")
        private WebElement home_logo;
        /*
        * 操作头
        * */
        //
        public void click_home_page(){

        }
        //培训点击操作
        public void click_Train(){
          Train.click();
        }
        //

}
