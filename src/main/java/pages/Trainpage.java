package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
* 后台左侧点击栏----继承后台头选项栏
*
* */
public class Trainpage extends Basepage{
    /*
    *我的教学
    * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[1]/a")
    public WebElement my_teaching;
    /*
     *在教课程
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[1]/ul/li[1]/a")
    public WebElement Teaching_courses;
    /*
     *在教专题
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[1]/ul/li[2]/a")
    public WebElement Teaching_topics;
    /*
     *培训项目教学
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[1]/ul/li[3]/a")
    public WebElement Training_Project_Teaching;
    /*
     *线上课程
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[2]/a")
    public WebElement Online_course;
    /*
     *线上课程库
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[2]/ul/li[1]/a")
    public WebElement Online_Course_Library;
    /*
     *线上课程专题
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[2]/ul/li[2]/a")
    public WebElement Online_Course_Topics;
    /*
     *标签管理
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[2]/ul/li[3]/a")
    public WebElement Label_management;
    /*
     *培训项目
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[3]/a")
    public WebElement Training_project;
    /*
     *专项考试管理
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[4]/a")
    public WebElement Special_Examination_Management;
    /*
     *问卷管理
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[5]/a")
    public WebElement Questionnaire_management;
    /*
     *问卷调查
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[6]/a")
    public WebElement Questionnaire_investigation;
    /*
     *线下活动
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[7]/a")
    public WebElement Offline_activities;
    /*
     *讲师管理
     * */
    @FindBy(xpath = "/html/body/div[2]/div[1]/ul/li[8]/a")
    public WebElement Lecturer_management;
    //-------------------------------方法-------------------------
    //点击我的教学
    public void click_my_teaching(){
        my_teaching.click();
    }
    //点击在教课程
    public void click_Teaching_courses(){
        Teaching_courses.click();
    }
    //点击在教专题
    public void click_Teaching_topics(){
        Teaching_topics.click();
    }
    //点击培训项目教学
    public void click_Training_Project_Teaching(){
        Training_Project_Teaching.click();
    }
    //点击线上课程
    public void click_Online_course(){
        Online_course.click();
    }
    //点击线上课程库
    public void click_Online_Course_Library(){
        Online_Course_Library.click();
    }
    //点击线上课程专题
    public void click_Online_Course_Topics(){
        Online_Course_Topics.click();
    }
    //点击标签管理
    public void click_Label_management(){
        Label_management.click();
    }
    //点击培训项目
    public void click_Training_project(){
        Training_project.click();
    }
    //点击专项考试管理
    public void click_Special_Examination_Management(){
        Special_Examination_Management.click();
    }
    //点击问卷管理
    public void click_Questionnaire_management(){
        Questionnaire_management.click();
    }
    //点击问卷调查
    public void click_Questionnaire_investigation(){
        Questionnaire_investigation.click();
    }
    //点击线上活动
    public void click_Offline_activities(){
        Offline_activities.click();
    }
    //点击讲师管理
    public void click_Lecturer_management(){
        Lecturer_management.click();
    }
}
