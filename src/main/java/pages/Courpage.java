package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
*课程设置页
* */
public class Courpage {
    /*
     *课程图片
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[1]/a")
    public WebElement img;
    /*
     *预览
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[1]/div[2]/div[1]/a")
    public WebElement preview;
    /*
     *发布课程
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[1]/div[2]/div[2]/button")
    public WebElement Release_course;
    /*
     *返回课程主页
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[1]/div[2]/div/a")
    public WebElement Return_course;
    /*
     *基本信息
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[1]/li[2]/a")
    public WebElement Essential_information;
    /*
     *详细信息
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[1]/li[3]/a")
    public WebElement detailed_information;
    /*
     *封面图片
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[1]/li[4]/a")
    public WebElement cover_photo;
    /*
     *任务列表
     * */
    @FindBy(xpath = "//*[@id=\"step-1\"]/a")
    public WebElement task_list;
    /*
     *学习设置
     * */
    @FindBy(xpath = "//*[@id=\"step-2\"]/a")
    public WebElement Learning_settings;
    /*
     *题目管理
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[3]/li[2]/a")
    public WebElement Topic_management;
    /*
     *试卷管理
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[3]/li[3]/a")
    public WebElement Examination_management;
    /*
     *试卷批阅
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[3]/li[4]/a")
    public WebElement Examination_marking;
    /*
     *作业批阅
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[3]/li[5]/a")
    public WebElement Homework_approval;
    /*
     *课程文件
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[4]/li[2]/a")
    public WebElement Course_documents;
    /*
     *讲师设置
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[4]/div/li[1]/a")
    public WebElement Lecturer_setup;
    /*
     *讲师评价
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[4]/div/li[2]/a")
    public WebElement Lecturer_evaluation;
    /*
     *学员管理
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[4]/div/li[3]/a")
    public WebElement Student_management;
    /*
     *课程数据
     * */
    @FindBy(xpath = "//*[@id=\"step-0\"]/a")
    public WebElement Course_data;
    /*
     *弹题统计
     * */
    @FindBy(xpath = "//*[@id=\"content-container\"]/div[2]/div[1]/div/ul[4]/li[3]/a")
    public WebElement counting_statistics;
    //---------------------方法---------------------
    /*
     *发布课程
     * */
    public void click_Release_course(){
        Release_course.click();
    }
    /*
    * 预览课程
    * */
    public void click_preview(){
        preview.click();
    }
    /*
     *返回课程主页
     * */
    public void click_Return_course(){
        Return_course.click();
    }
    /*
     *基本信息
     * */
    public void click_Essential_information(){
        Essential_information.click();
    }
    /*
     *封面图片
     * */
    public void click_cover_photo(){
        cover_photo.click();
    }
    /*
     *详细信息
     * */
    public void click_detailed_information(){
        detailed_information.click();
    }
    /*
     *任务列表
     * */
    public void click_task_list(){
        task_list.click();
    }
    /*
     *学习设置
     * */
    public void click_Learning_settings(){
        Learning_settings.click();
    }
    /*
     *提目管理
     * */
    public void click_Topic_management(){
        Topic_management.click();
    }
    /*
     *试卷管理
     * */
    public void click_Examination_management(){
        Examination_management.click();
    }
    /*
     *试卷批阅
     * */
    public void click_Examination_marking(){
        Examination_marking.click();
    }
    /*
     *作业批阅
     * */
    public void click_Homework_approval(){
        Homework_approval.click();
    }
    /*
     *课程文件
     * */
    public void click_Course_documents(){
        Course_documents.click();
    }
    /*
     *讲师设置
     * */
    public void click_Lecturer_setup(){
        Lecturer_setup.click();
    }
    /*
     *讲师评价
     * */
    public void click_Lecturer_evaluation(){
        Lecturer_evaluation.click();
    }
    /*
     *学员管理
     * */
    public void click_Student_management(){
        Student_management.click();
    }
    /*
     *
     * 课程数据
     * */
    public void click_Course_data(){
        Course_data.click();
    } /*
     *弹题统计
     * */
    public void click_counting_statistics(){
        counting_statistics.click();
    }
}
