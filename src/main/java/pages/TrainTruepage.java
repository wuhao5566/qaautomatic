package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/*
* 培训-----在教课程页
*
* */
public class TrainTruepage extends Basepage{
    /*
    *普通课程
    * */
    @FindBy(xpath = "//*[@id=\"ct-nav-tabs_data\"]/li[1]/a")
    public WebElement General_course;
    /*
     *直播课程
     * */
    @FindBy(xpath = "//*[@id=\"ct-nav-tabs_data\"]/li[2]/a")
    public WebElement Live_broadcast_course;
    /*
     *专题课程
     * */
    @FindBy(xpath = "//*[@id=\"ct-nav-tabs_data\"]/li[3]/a")
    public WebElement Thematic_curriculum;
    /*
     *创建课程
     * */
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[2]/div/div/a")
    public WebElement Create_Curriculum;
    /*
     *课程名称
     * */
    @FindBy(xpath = "")
    public WebElement Curriculum_name;
    /*
     *教评详情
     * */
    @FindBy(xpath = "")
    public WebElement Teaching_valuation_details;
    /*
     *管理
     * */
    @FindBy(xpath = "")
    public WebElement Management_course;
    /*
     *首页
     * */
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[2]/nav/nav/ul/li[1]/a")
    public WebElement home_page_butten;
    /*
     *上一页
     * */
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[2]/nav/nav/ul/li[2]/a")
    public WebElement last_page;
    /*
     *下一页
     * */
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[2]/nav/nav/ul/li[3]/a")
    public WebElement next_page;
    /*
     *尾页
     * */
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[2]/nav/nav/ul/li[4]/a")
    public WebElement Tail_page;
    //--------------------------方法---------------------------------------
    public void click_Create_Curriculum(){
        Create_Curriculum.click();
    }

}
